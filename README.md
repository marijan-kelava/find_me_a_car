## Setup

# Find me a car

This simple app is multilingual database consisted of cars, features, categories and tags which can be searched by different criteria.

## Installation

Clone repository `git clone https://gitlab.com/marijan-kelava/find_me_a_car.git your-project`
Enter to project folder `cd your-project`
Checkout to master branch

## Docker Setup
 - create .env and copy contents of env.local
 - build docker containers `docker-compose build`
 - run `docker-compose up -d` to build up the containers 
 - login to `find_me_a_car_web` container `docker exec -it find_me_a_car_web bash` 
 - run commands:
    `composer install` ,
    `php bin/console do:sc:dr --force`,
    `php bin/console do:sc:cr`

- to load fixtures:
    `php bin/console do:fi:lo`

## Default database credentials:
 - server: find_me_a_car_db
 - username: user
 - password: user
 - database: db

## Request URL example
http://localhost:8888/meals/v1?per_page=10&page=1&lang=hr&with=category,tags,ingridients&diff_time=1987654321

http://localhost:8888/meals/v1?per_page=10&page=1&lang=en&with=category,tags,ingridients&diff_time=1987654321








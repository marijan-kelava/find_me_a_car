<?php

namespace App\Repository;

use App\Entity\Car;
use Doctrine\ORM\Query;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query as ORMQuery;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Car|null find($id, $lockMode = null, $lockVersion = null)
 * @method Car|null findOneBy(array $criteria, array $orderBy = null)
 * @method Car[]    findAll()
 * @method Car[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CarRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Car::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Car $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Car $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return Car[] Returns an array of Car objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Car
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAll(): array
    {
        $qb = $this->createQueryBuilder('c');
        $qb->leftJoin('c.contents', 'con')
            ->addSelect('con');

        $query = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
        //dd($query);
        return $query;
    }

    public function findOne($id)
    {
        /*$conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT * FROM car';

        $sql .= ' LEFT JOIN content ON car.id = content.entity_id WHERE car.id = :id';
        
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id', $id);

        $resultSet = $stmt->executeQuery();
        //dd($resultSet->fetchAllAssociative());
        return $resultSet->fetchAllAssociative();*/

        $qb = $this->createQueryBuilder('c');
        $qb->leftJoin('c.contents', 'con')
           ->addSelect('con');
        $qb->andWhere('c.id IN (:id)')
           ->setParameter('id', $id);

        $query = $qb->getQuery()->getResult();
        
        return $query;
    }
        
    public function getAllAboutCar(int $id)
    {
        $qb = $this->createQueryBuilder('c');
        //dd($parameters);
        $qb->leftJoin('c.contents', 'con')
           ->addSelect('c.id, c.createdAt, c.updatedAt, c.deletedAt, con.title, con.description');
           //->addSelect('con.title');
           $qb->andWhere('con.fqcn = :name')
           ->setParameter('name', 'App\Entity\Car');
           //->orderBy('c.id', 'ASC');
        
        $qb->leftJoin('c.category', 'cat')
            ->addSelect('cat');
        $qb->leftJoin('cat.contents', 'cont')
            ->addSelect('cont');
        $qb->andWhere('cat.id IN (:id)')
            ->setParameter('id', $id);

        $qb->leftJoin('c.tags', 'tag')
            ->addSelect('tag');
        $qb->leftJoin('tag.contents', 'conte')
            ->addSelect('conte');
        $qb->andWhere('tag.id IN (:id)')
            ->setParameter('id', $id);
        $qb->andWhere('tag.id = conte.entityId');
    
        $qb->leftJoin('c.features', 'feat')
            ->addSelect('feat');
        $qb->leftJoin('feat.contents', 'content')
            ->addSelect('content')
            ->andWhere('feat.id = content.entityId');
        
        $query = $qb->getQuery()->setHydrationMode(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
        //dd($query);
        return $query;
    }           

    public function getCarsByCriteria(array $parameters)
    {
        $qb = $this->createQueryBuilder('c');
        //dd($parameters);
        $qb->leftJoin('c.contents', 'con')
           ->addSelect('c.id, c.createdAt, c.updatedAt, c.deletedAt, con.title, con.description');
           $qb->andWhere('con.fqcn = :name')
           ->setParameter('name', 'App\Entity\Car');
           //->orderBy('c.id', 'ASC');

        if (isset($parameters['lang'])) {
            /*$qb->andWhere('con.languageId = :lang')
            ->setParameter('lang', $parameters['lang']);*/
            $qb->andWhere('con.languageIsoCode = :lang')
            ->setParameter('lang', $parameters['lang']);
        }
        
        if (in_array('category', $parameters['with'])) {
            $qb->leftJoin('c.category', 'cat')
               ->addSelect('cat');
            $qb->leftJoin('cat.contents', 'cont')
               ->addSelect('cont');

            if (isset($parameters['category'])) {
                $categoryId = explode(',', $parameters['category']);
                $qb->andWhere('cat.id IN (:id)')
                   ->setParameter('id', $categoryId);
            }
            
            $qb->andWhere('cat.id = cont.entityId')
               /*->andWhere('cont.languageId = :lang')
               ->setParameter('lang', $parameters['lang']);*/
               ->andWhere('cont.languageIsoCode = :lang')
               ->setParameter('lang', $parameters['lang']);

        }

        if(in_array('tags', $parameters['with'])) {
            $qb->leftJoin('c.tags', 'tag')
               ->addSelect('tag');
               $qb->leftJoin('tag.contents', 'conte')
               ->addSelect('conte');

               if (isset($parameters['tags'])) {
                $tagsId = explode(',', $parameters['tags']);
                $qb->andWhere('tag.id IN (:id)')
                   ->setParameter('id', $tagsId);
            }

            $qb->andWhere('tag.id = conte.entityId')
               /*->andWhere('conte.languageId = :lang')
               ->setParameter('lang', $parameters['lang']);*/
               ->andWhere('conte.languageIsoCode = :lang')
               ->setParameter('lang', $parameters['lang']);
        }

        if(in_array('features', $parameters['with'])) {
            $qb->leftJoin('c.features', 'feat')
               ->addSelect('feat');
            $qb->leftJoin('feat.contents', 'content')
               ->addSelect('content')
               ->andWhere('feat.id = content.entityId')
               /*->andWhere('content.languageId = :lang')
               ->setParameter('lang', $parameters['lang']);*/
               ->andWhere('content.languageIsoCode = :lang')
               ->setParameter('lang', $parameters['lang']);
        }
        
        $query = $qb->setMaxResults($parameters['limit'])->setFirstResult($parameters['offset'])->getQuery()->setHydrationMode(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
        //dd($query);
        return $query;
    }

    public function getCarsCount()
    {
        return $this->createQueryBuilder('c')
        ->select('count(c.id)')
        ->getQuery()
        ->getSingleScalarResult();
    }

    public function findMaxId(): ?int
    {
        $maxId = $this->createQueryBuilder('c')
            ->select('MAX(c.id)')
            ->getQuery()
            ->getSingleScalarResult();
            return $maxId;
    }
}

<?php

namespace App\Form;

use App\Entity\Content;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'attr' => array(
                    'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => 'Enter car title...',
                ),
                'label' => false
            ])
            ->add('description', TextareaType::class, [
                'attr' => array(
                    'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => 'Enter car description...',
                ),
                'label' => false
            ])
            ->add('imagePath', FileType::class, [
                //'mapped' => false,
                'attr' => array(
                    'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => 'Upload image...',
                ),
                'label' => false
            ])
            ->add('languageIsoCode', ChoiceType::class, [
                'choices' => [
                    'Language' => [
                        'en' => 'en',
                        'hr' => 'hr',
                    ]
                ],
                'attr' => array(
                    'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => 'Enter language...',
                ),
                'label' => false
            ])
            /*->add('cars', CollectionType::class, [
                'entry_type' => CarFormType::class,
                'attr' => array(
                    'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => 'Enter car slug...',
                ),
                'label' => false
            ])*/
            /*->add('category_title', TextType::class, [
                'mapped' => false,
                'attr' => array(
                    'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => 'Enter category title...',
                ),
                'label' => false
            ])*/
            ->add('category', CollectionType::class, [
                'entry_type' => CategoryFormType::class,
                'attr' => array(
                    'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => 'Enter category slug...',
                ),
                'label' => false
            ])
            ->add('tags', CollectionType::class, [
                'entry_type' => TagFormType::class,
                'attr' => array(
                    'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => 'Enter tag slug...',
                ),
                'label' => false
            ])
            ->add('features', CollectionType::class, [
                'entry_type' => FeatureFormType::class,
                'attr' => array(
                    'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => 'Enter feature slug...',
                ),
                'label' => false
            ])
            
            //->add('description')
            //->add('fqcn')
            //->add('languageId')
            //->add('languageIsoCode')
            //->add('tags')
            //->add('category')
            //->add('ingridients')
            //->add('languages')
            //->add('meals')
            //->add('cars')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Content::class,
            'allow_extra_fields' => true,
            'allow_add' => true,
        ]);
    }
}

<?php

namespace App\Form;

use App\Entity\Car;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class CarFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('category_title', ChoiceType::class, [
            'mapped' => false,
            'choices' => [
                'Category' => [
                    'Hatchback' => 'hatchback',
                    'Sedan' => 'Sedan',
                    'Coupe' => 'coupe',
                    'SUV' => 'suv'
                ]
            ],
            'attr' => array(
                'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                'placeholder' => 'Enter car category',
            ),
            'label' => false
        ])
        ->add('engine_title', ChoiceType::class, [
            'mapped' => false,
            'choices' => [
                'Category' => [
                    'Petrol' => 'petrol',
                    'Diesel' => 'diesel',
                    'Hybrid' => 'hybrid',
                    'Electric' => 'electric'
                ]
            ],
            'attr' => array(
                'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                'placeholder' => 'Engine',
            ),
            'label' => false
        ])
        ->add('transmission_title', ChoiceType::class, [
            'mapped' => false,
            'choices' => [
                'Category' => [
                    'Manual' => 'manual',
                    'Automatic' => 'automatic',
                    'Electric' => 'electric'
                ]
            ],
            'attr' => array(
                'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                'placeholder' => 'Transmission',
            ),
            'label' => false
        ])
        ->add('mileage_title', TextType::class, [
            'mapped' => false,
            'attr' => array(
                'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                'placeholder' => 'Mileage',
            ),
            'label' => false
        ])
        ->add('price_title', TextType::class, [
            'mapped' => false,
            'attr' => array(
                'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                'placeholder' => 'Price',
            ),
            'label' => false
        ])
        /*->add('imagePath', FileType::class, [
            'mapped' => false,
            'attr' => array(
                'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                'placeholder' => 'Upload image...',
            ),
            'label' => false
        ])*/
            ->add('contents', CollectionType::class, [
                'entry_type' => ContentFormType::class,
                'attr' => array(
                    'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => '',
                ),
                'label' => false
            ])
            /*->add('slug', TextType::class, [
                'attr' => array(
                    'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => 'Enter car slug...',
                ),
                'label' => false
            ])*/
            /*->add('category', CollectionType::class, [
                'entry_type' => CategoryFormType::class,
                'attr' => array(
                    'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => '',
                ),
                'label' => false
            ])*/
            /*->add('tags', CollectionType::class, [
                'entry_type' => TagFormType::class,
                'attr' => array(
                    'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => '',
                ),
                'label' => false
            ])*/
            /*->add('features', CollectionType::class, [
                'entry_type' => FeatureFormType::class,
                'attr' => array(
                    'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => '',
                ),
                'label' => false
            ])*/
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Car::class,
            'allow_extra_fields' => true,
        ]);
        //dd($resolver->getDefinedOptions());
    }
}

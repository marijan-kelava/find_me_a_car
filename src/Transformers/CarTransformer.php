<?php

namespace App\Transformers;

final class CarTransformer
{
    public function transformCars(array $cars): array
    {
        $data = [];
        $y = 0;
        //dd($cars);
        foreach ($cars as $key => $car) {
            
            $data[$y]['id'] = $car['id'];
            $data[$y]['title'] = $car['title'];
            $data[$y]['description'] = $car['description'];
            $data[$y]['createdAt'] = $car['createdAt']->format('Y-m-d H:i:s') ?: 'null';

            $data[$y]['updatedAt'] = ($car['updatedAt'] !== null) ? $car['updatedAt']->format('Y-m-d H:i:s') : null;

            $data[$y]['deletedAt'] = ($car['deletedAt'] !== null) ? $car['deletedAt']->format('Y-m-d H:i:s') : null;
            
            if (isset($car[0]['category'][0])) {
                $data[$key]['category']['id'] = $car[0]['category'][0]['id'];  
                $data[$key]['category']['title'] = $car[0]['category'][0]['contents'][0]['title'];  
                $data[$key]['category']['slug'] = $car[0]['category'][0]['slug']; 
            }
            
            if (isset($car[0]['tags'][0])) {
                $data[$key]['tags']['id'] = $car[0]['tags'][0]['id'];  
                $data[$key]['tags']['title'] = $car[0]['tags'][0]['contents'][1]['title'];  
                $data[$key]['tags']['slug'] = $car[0]['tags'][0]['slug']; 
            }
            
            if (isset($car[0]['features'][0])) {

                $x = count($cars[$y][0]['features'][0]['contents']);
                //dd($x);
                for($i = 2; $i < $x; $i++){
                $data[$key]['features'][$i - 2]['id'] = $car[0]['features'][0]['id'];  
                $data[$key]['features'][$i - 2]['title'] = $car[0]['features'][0]['contents'][$i]['title']; 
                $data[$key]['features'][$i - 2]['slug'] = $car[0]['features'][0]['slug'];
                }; 
            }

            $y++;
        }

        return $data;
    }             

    public function transformResults(array $cars): array
    {
        $y = 0;
        $data = [];
        //dd($cars);
        foreach ($cars as $key => $car) {
            $data[$key]['id'] = $car['id'];    
            $data[$key]['title'] = $car['title'];  
            $data[$key]['description'] = $car['description'];
            $data[$key]['created'] = $car['createdAt']->format('Y-m-d H:i:s') ?: 'null';
            $data[$key]['updated'] = ($car['updatedAt'] !== null) ? $car['updatedAt']->format('Y-m-d H:i:s') : null;
        
            if (isset($car[0]['category'][0])) {
                $data[$key]['category']['id'] = $car[0]['category'][0]['id'];  
                $data[$key]['category']['title'] = $car[0]['category'][0]['contents'][0]['title'];  
                $data[$key]['category']['slug'] = $car[0]['category'][0]['slug']; 
            }
            
            if (isset($car[0]['tags'][0])) {
                $data[$key]['tags']['id'] = $car[0]['tags'][0]['id'];  
                $data[$key]['tags']['title'] = $car[0]['tags'][0]['contents'][0]['title'];  
                $data[$key]['tags']['slug'] = $car[0]['tags'][0]['slug']; 
            }
            
            if (isset($car[0]['features'][0])) {

                $y = count($cars[$y][0]['features'][0]['contents']);
                
                //$y = count($cars[$y][0]['features'][0]['contents'][2,3,4,5]);
                //dd($y);
                //$data[$key]['features'][0]['id'] = $car[0]['features'][0]['id'];
                //$data[$key]['features'][0]['slug'] = $car[0]['features'][0]['slug'];
                for($i = 0; $i < $y; $i++){
                    /*if ($i < 1) {
                        $data[$key]['features'][$i]['id'] = $car[0]['features'][$i]['id'];
                        $data[$key]['features'][$i]['slug'] = $car[0]['features'][$i]['slug'];
                        $data[$key]['features'][$i]['title'] = $car[0]['features'][$i]['title'];
                    }*/
                //dd($car);
                $data[$key]['features'][$i]['id'] = $car[0]['features'][0]['id'];  
                $data[$key]['features'][$i]['title'] = $car[0]['features'][0]['contents'][$i]['title'];
                $data[$key]['features'][$i]['slug'] = $car[0]['features'][0]['slug'];
                }; 
            }
            $y++;

        }
        //dd($data);
        return $data;
    }
}
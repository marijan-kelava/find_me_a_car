<?php

namespace App\Dto;

final class CarDto
{
    /**
     * @Assert\NotBlank
     */
    private string $slug;

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
<?php

namespace App\Services;

use App\Entity\Car;
use App\Entity\Content;
use App\Repository\CarRepository;
use App\Repository\ContentRepository;
use App\Transformers\CarTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;
use Symfony\Component\String\Slugger\SluggerInterface;

final class ContentService
{
    private $em;
    private CarRepository $carRepository;
    private ContentRepository $contentRepository;
    private CarTransformer $carTransformer;
    private SluggerInterface $slugger;

    public function __construct(
        EntityManagerInterface $em,
        CarRepository $carRepository,
        ContentRepository $contentRepository,
        CarTransformer $carTransformer,
        SluggerInterface $slugger
        )
    {
        $this->em = $em;
        $this->carRepository = $carRepository;
        $this->contentRepository = $contentRepository;
        $this->carTransformer = $carTransformer;
        $this->slugger = $slugger;
    }

    /*public function getCars(): array
    {
        $data = $this->carRepository->findAll();
        $data = $this->carTransformer->transformCars($data);
        
        return $data;
    }*/

    /*public function getCar($id)
    {
        $data = $this->carRepository->findOne($id);
        //$data = (!empty($data)) ? $data[0] : new Car();
        try {
            if (empty($data) || $data[0]->getId() == null) {
                throw new Exception("Car not found, please enter valid car id in the query");
            }
        } catch (\Exception $e) {
            echo "Error: " . $e->getMessage();
            //return $this->redirectToRoute('app_car_show');
            exit;
        }

        //$data = $this->carTransformer->transformCars($data);
        
        return $data;
    }*/

    /*public function getAllAboutCar($id)
    {
        $data = $this->carRepository->findAllAboutCar($id);
        //$data = (!empty($data)) ? $data[0] : new Car();
        try {
            if (empty($data) || $data[0]['id'] == null) {
                throw new Exception("Car not found, please enter valid car id in the query");
            }
        } catch (\Exception $e) {
            echo "Error: " . $e->getMessage();
            //return $this->redirectToRoute('app_car_show');
            exit;
        }
        $data = $this->carTransformer->transformCars($data);

        return $data;
    }*/

    public function saveContent(Content $content)
    {
        $this->em->persist($content);
        $this->em->flush();
        return $content;
    }

    public function deleteContent(Content $content)
    {
        $this->contentRepository->remove($content);
    }

    public function prepareData($content, $maxId, $categoryTitle, $category): array
    {
        if($maxId === null) {
            $content->setEntityId(1);
            $content->setFqcn(Content::class);
        } else {
            $content->setEntityId($maxId +1);
            $content->setFqcn(Content::class);
        }

        if ($content->getLanguageIsoCode() == 'en') {
            $content->setLanguageId(1);
        } else {
            $content->setLanguageId(2);
        }
        
        $categorySlug = $this->generateSlug($categoryTitle);
        $category->setSlug($categorySlug);
        $content->addCategory($category);

        $content1 = new Content();
        //$categoryTitle = $form->get('category_title')->getData();
        $content1->setEntityId($maxId + 1);
        $content1->setTitle($categoryTitle);
        $content1->setLanguageId($content->getLanguageId());
        $content1->setLanguageIsoCode($content->getLanguageIsoCode());
        $content1->setFqcn(Category::class);

        return [$content, $content1];
    }

    private function generateSlug($title)
    {
        $slug = $this->slugger->slug($title)->lower();
        return $slug;
    }
}
<?php

namespace App\Services;

use App\Entity\Car;
use App\Entity\Content;
use App\Repository\CarRepository;
use App\Repository\ContentRepository;
use App\Transformers\CarTransformer;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;
use Symfony\Component\String\Slugger\SluggerInterface;
use App\Services\PaginatorService;

final class CarService
{
    private $em;
    private CarRepository $carRepository;
    private ContentRepository $contentRepository;
    private CarTransformer $carTransformer;
    private PaginatorService $paginator;
    private SluggerInterface $slugger;

    public function __construct(
        EntityManagerInterface $em,
        CarRepository $carRepository,
        ContentRepository $contentRepository,
        CarTransformer $carTransformer,
        PaginatorService $paginator,
        SluggerInterface $slugger
        )
    {
        $this->em = $em;
        $this->carRepository = $carRepository;
        $this->contentRepository = $contentRepository;
        $this->carTransformer = $carTransformer;
        $this->paginator = $paginator;
        $this->slugger = $slugger;
    }

    public function getCars(array $parameters): array
    {
        $parameters['limit'] = isset($parameters['per_page']) ? (int) $parameters['per_page'] : null;
        
        $parameters['offset'] = isset($parameters['page']) && $parameters['limit'] !== null ? ((int) $parameters['page'] - 1) * $parameters['limit'] : null;        
        
        $parameters['with'] = $this->_configureWithParameters($parameters['with'] ?? null);
        //dd($parameters);

        $query = $this->carRepository->getCarsByCriteria($parameters);
        
        $pagination = $this->paginator->paginate($query);
        dd($pagination);
        $totalItems = (int) $pagination['total'];
        
        $itemsPerPage = $parameters['limit'] ?? $totalItems;
        //dd($pagination);
        $transformedCars = $this->carTransformer->transformCars($pagination['dataAsArray'], $parameters);
        
        $data['meta']['currentPage'] = $parameters['page'] ?? null;
        $data['meta']['totalItems'] = $totalItems;
        $data['meta']['itemsPerPage'] = $itemsPerPage;
        $data['meta']['totalPages'] = (int) ceil($totalItems / $itemsPerPage);
        $data['data'] = $transformedCars;
        
        return $data;
    }

    private function _configureWithParameters(?string $param = null) : ?array
    {

        if ($param === null){
           return ['category', 'tags', 'features'];
        }
        
        return explode(',', $param);
    }

    public function showCars()
    {
        $data = $this->carRepository->findAll();
        //dd($data[4]);
        $cars = [];
        foreach ($data as $key => $value) {
            $cars[$key]['id'] = $data[$key]['id'];
            $cars[$key]['created'] = ($data[$key]['createdAt'])->format('Y-m-d H:i:s');
            if (!empty($data[$key]['updatedAt'])) {
                $cars[$key]['updated'] = ($data[$key]['updatedAt'])->format('Y-m-d H:i:s');
            }
            else {
                $cars[$key]['updated'] = '';
            }
            $cars[$key]['title'] = $data[$key]['contents'][0]['title'];
            $cars[$key]['category'] = $data[$key]['contents'][1]['title'];
            $cars[$key]['mileage'] = $data[$key]['contents'][5]['title'];
            $cars[$key]['price'] = $data[$key]['contents'][6]['title'];
            $cars[$key]['image'] = $data[$key]['contents'][0]['imagePath'];
        }
        //dd($cars);
        return $cars;
    }

    public function getCarDetails($id)
    {
        $data = $this->carRepository->getAllAboutCar($id);
        
        $pagination = $this->paginator->paginate($data);
        //dd($pagination);
        $car = $this->carTransformer->transformCars($pagination['dataAsArray']);
        //dd($car);
        return $car;
    }

    public function prepareUpdate($car, $mileage, $price)
    {
        $date = new DateTimeImmutable();
        //dd($car);
        $car->setUpdatedAt($date);
        
        $contents = $car->getContents();
        
        $collection = $contents->toarray();
        //dd($collection);
        $collection[5]->setTitle($mileage);
        $collection[6]->setTitle($price);

        return $car;
    }

    public function saveCar($car)
    {
        $this->em->persist($car);
        $this->em->flush();
        return $car;
    }

    public function deleteCar(Car $car)
    {
        $this->carRepository->remove($car);
    }

    public function prepareData($car, $maxId, $form, $content, $category, $tag, $feature): array
    {   
        $data = [];

        $carTitle = $car->getContents()[0]->getTitle();
        $categoryTitle = $form->get('category_title')->getData();

        $carSlug = $this->generateSlug($categoryTitle);
        $car->setSlug($carSlug);
        
        $tagSlug = $this->generateSlug($carTitle);
        $tag->setSlug($tagSlug);

        $engineTitle = $form->get('engine_title')->getData();
        $transmissionTitle = $form->get('transmission_title')->getData();
        $mileageTitle = $form->get('mileage_title')->getData();
        $priceTitle = $form->get('price_title')->getData();

        $featureSlug = $this->generateTitle($engineTitle, $transmissionTitle);
        $feature->setSlug($featureSlug);

        if (empty($maxId)) {
            $content->setEntityId(1);
        }else {
            $content->setEntityId($maxId + 1);
        }

        $languageIsoCode = (($form->get('contents')->getData())[0])->getlanguageIsoCode();
            
            if ($languageIsoCode == 'en') {
                $content->setLanguageId(1);
            } else {
                $content->setLanguageId(2);
            }
            $content->setFqcn(Car::class);

        
        $categorySlug = $this->generateSlug($categoryTitle);
        $category->setSlug($categorySlug);
        
        $content1 = new Content();
        $content1->setEntityId($maxId + 1);
        $content1->setTitle($categoryTitle);
        $content1->setLanguageId($content->getLanguageId());
        $content1->setLanguageIsoCode($content->getLanguageIsoCode());
        $content1->setFqcn('App\Entity\Category');
        $content1->addCategory($category);
        $content1->addTag($tag);
        $content1->addFeature($feature);
        $car->addContent($content1);

        $content2 = new Content();
        $content2->setEntityId($maxId + 1);
        $tagTitle = $this->generateTitle($carTitle, $categoryTitle);
        $content2->setTitle($tagTitle);
        $content2->setLanguageId($content->getLanguageId());
        $content2->setLanguageIsoCode($content->getLanguageIsoCode());
        $content2->setFqcn('App\Entity\Tag');
        $content2->addCategory($category);
        $content2->addTag($tag);
        $content2->addFeature($feature);
        $car->addContent($content2);

        $content3 = new Content();
        $content3->setEntityId($maxId + 1);
        $featureTitle = $this->generateTitle($engineTitle, $transmissionTitle);
        $content3->setTitle($engineTitle);
        $content3->setLanguageId($content->getLanguageId());
        $content3->setLanguageIsoCode($content->getLanguageIsoCode());
        $content3->setFqcn('App\Entity\Feature');
        $content3->addCategory($category);
        $content3->addTag($tag);
        $content3->addFeature($feature);
        $car->addContent($content3);

        $content4 = new Content();
        $content4->setEntityId($maxId + 1);
        $featureTitle = $this->generateTitle($engineTitle, $transmissionTitle);
        $content4->setTitle($transmissionTitle);
        $content4->setLanguageId($content->getLanguageId());
        $content4->setLanguageIsoCode($content->getLanguageIsoCode());
        $content4->setFqcn('App\Entity\Feature');
        $content4->addCategory($category);
        $content4->addTag($tag);
        $content4->addFeature($feature);
        $car->addContent($content4);

        $content5 = new Content();
        $content5->setEntityId($maxId + 1);
        $content5->setTitle($mileageTitle);
        $content5->setLanguageId($content->getLanguageId());
        $content5->setLanguageIsoCode($content->getLanguageIsoCode());
        $content5->setFqcn('App\Entity\Feature');
        $content5->addCategory($category);
        $content5->addTag($tag);
        $content5->addFeature($feature);
        $car->addContent($content5);

        $content6 = new Content();
        $content6->setEntityId($maxId + 1);
        $content6->setTitle($priceTitle);
        $content6->setLanguageId($content->getLanguageId());
        $content6->setLanguageIsoCode($content->getLanguageIsoCode());
        $content6->setFqcn('App\Entity\Feature');
        $content6->addCategory($category);
        $content6->addTag($tag);
        $content6->addFeature($feature);
        $car->addContent($content6);

        $data = [$car, $content1, $content2, $content3, $content4, $content5, $content6];

        return $data;
    }

    private function generateSlug($title)
    {
        $slug = $this->slugger->slug($title)->lower();
        return $slug;
    }

    private function generateTitle($carSlug, $categoryTitle)
    {
        $tagTitle = $carSlug . '-' . $categoryTitle;

        $tagTitle = strtolower(str_replace(' ', '-', $tagTitle));

        return $tagTitle;
    }
}
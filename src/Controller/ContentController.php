<?php

namespace App\Controller;

use App\Entity\Car;
use App\Entity\Tag;
use DateTimeImmutable;
use App\Entity\Content;
use App\Entity\Feature;
use App\Entity\Category;
use App\Form\CarFormType;
use App\Form\ContentFormType;
use App\Form\SearchFormType;
use App\Services\CarService;
use App\Services\ContentService;
use App\Repository\CarRepository;
use App\Repository\ContentRepository;
use App\Transformers\CarTransformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContentController extends AbstractController
{
    private ContentService $contentService;
    private CarTransformer $carTransformer;
    private ContentRepository $contentRepository;

    public function __construct(ContentService $contentService, CarTransformer $carTransformer, ContentRepository $contentRepository)
    {
        $this->contentService = $contentService;
        $this->carTransformer = $carTransformer;
        $this->contentRepository = $contentRepository;
    }

    /**
     * @Route("/content", name="app_content")
     */
    public function index(): Response
    {
        return $this->render('content/index.html.twig', [
            'controller_name' => 'ContentController',
        ]);
    }

    /**
     * @Route("/content/create", name="app_content_create")
     */
    public function create(Request $request, ContentService $contentService, ContentRepository $contentRepository): Response
    {
        $car = new Car();
        $content = new Content();
        $category = new Category();
        $tag = new Tag();
        $feature = new Feature();

        $content->addCar($car);
        $content->addCategory($category);
        $content->addTag($tag);
        $content->addFeature($feature);
        
        $form = $this->createForm(ContentFormType::class, $content);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $content = $form->getData();
            //dd($content);
            $categoryTitle = $form->get('category_title')->getData();
            $maxId = $contentRepository->findMaxId();
            $category->setSlug('electric');
            
            $preparedData = $contentService->prepareData($content, $maxId, $categoryTitle, $category);
            //dd($preparedData);
            $savedContent = $contentService->saveContent($preparedData[0]);
            
            return $this->redirectToRoute('app_car_show');
        }

        return $this->render('content/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}

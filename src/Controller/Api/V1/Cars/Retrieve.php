<?php

namespace App\Controller\Api\V1\Cars;

use App\Services\CarService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

final class Retrieve extends AbstractController
{
    private CarService $carService;

    public function __construct(
        CarService $carService
        )
    {
        $this->carService = $carService;
    }

    /**
     * @Route("/cars/v1", name="cars_show", methods={"GET"})
     */
    public function getCars(Request $request) : JsonResponse
    {   
        //$parameters = [];
        //$parameters['per_page'] = 10;
        //dd($parameters);
        $parameters = $request->query->all();
        //dd($parameters);
        $data = $this->carService->getCars($parameters);
        dd($data);
        return $this->json($data);
    }
}
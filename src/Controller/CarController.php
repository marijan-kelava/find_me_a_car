<?php

namespace App\Controller;

use App\Entity\Car;
use App\Entity\Tag;
use DateTimeImmutable;
use App\Entity\Content;
use App\Entity\Feature;
use App\Entity\Category;
use App\Form\CarFormType;
use App\Form\ContentFormType;
use App\Form\SearchFormType;
use App\Form\UpdateFormType;
use App\Services\CarService;
use App\Services\ContentService;
use App\Repository\CarRepository;
use App\Repository\ContentRepository;
use App\Services\ImageService;
use App\Transformers\CarTransformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CarController extends AbstractController
{
    private CarService $carService;
    private CarTransformer $carTransformer;
    private CarRepository $carRepository;

    public function __construct(CarService $carService, CarTransformer $carTransformer, CarRepository $carRepository)
    {
        $this->carService = $carService;
        $this->carTransformer = $carTransformer;
        $this->carRepository = $carRepository;
    }

    /**
     * @Route("/car/show", name="app_car_show")
     */
    public function show(Request $request, CarRepository $carRepository): Response
    {
        $cars = $this->carService->showCars();
        //dd($cars);
        return $this->render('car/show.html.twig', [
            'cars' => $cars,
        ]);
    }

    /**
     * @Route("/show/details/{id}", name="app_show_details", defaults={"id"=null})
     */
    public function showDetails($id): Response
    {
        $car = $this->carService->getCarDetails($id);
        //dd($car);
        return $this->render('car/details.html.twig', [
            'car' => $car,
        ]);
    }

    /**
     * @Route("/car/create", name="app_car_create")
     */
    public function create(Request $request, CarService $carService, CarRepository $carRepository, ImageService $imageService): Response
    {
        $car = new Car();
        $content = new Content();
        $category = new Category();
        $tag = new Tag();
        $feature = new Feature();

        $car->addContent($content);
        $car->addCategory($category);
        $car->addTag($tag);
        $car->addFeature($feature);
        
        $form = $this->createForm(CarFormType::class, $car);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            //dd($form->getData());
            $car = $form->getData();
            //dd($car);
            
            $uploadedFile = $car->getContents()[0]->getImagePath();
            //dd($uploadedFile);
            $image = $imageService->handleImageUpload($uploadedFile);
            //dd($uploadedFile);
            //dd($image);

            $maxId = $carRepository->findMaxId();
            
            $preparedData = $carService->prepareData($car, $maxId, $form, $content, $category, $tag, $feature);

            foreach ($preparedData as $data) {
                $carService->saveCar($data);
            }
            
            return $this->redirectToRoute('app_car_show');
        }

        return $this->render('car/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/car/update/{id}", name="app_car_update", defaults={"id"=null}, methods={"GET", "POST"}))
     */
    public function update(Request $request, CarService $carService, CarRepository $carRepository, $id): Response
    {   
        $car = $carRepository->findOne($id);
        $car = $car[0];
        //dd($car);
        $defaultData = ['message' => 'Mileage and price'];
        $form = $this->createFormBuilder($defaultData)
            ->add('mileage', TextType::class)
            ->add('price', TextType::class)
            ->getForm();
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            
            $mileage = $data['mileage'];
            $price = $data['price'];

            $carService->prepareUpdate($car, $mileage, $price);

            $carService->saveCar($car);

            $this->addFlash('succes', "Car was successfully updated");

            return $this->redirectToRoute('app_car_show');
        }
        
        return $this->render('car/update.html.twig', [
            'form' => $form->createView()
        ]);
    }

     /**
     * @Route("/car/delete/{id}", name="app_car_delete", defaults={"id"=null}, methods={"GET", "DELETE"}))
     */
    public function delete(Request $request, CarService $carService, CarRepository $carRepository, $id): Response
    {
        $result = $carRepository->findOne($id);
        $car = $result[0];
        //dd($car);
        $carService->deleteCar($car);

        return $this->redirectToRoute('app_car_show');
    }
}
